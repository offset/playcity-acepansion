/*
** playcity.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H


#define LIBNAME "playcity.acepansion"
#define VERSION 1
#define REVISION 2
#define DATE "21.10.2023"
#define COPYRIGHT "� 2020-2023 Philippe Rimauro"

#define API_VERSION 7


#endif /* ACEPANSION_H */

