#include "Z84C30.h"

#define CTRL_CONTROL_VECTOR 0x01
#define CTRL_RESET          0x02
#define CTRL_TIME_CST       0x04
#define CTRL_TIMER          0x08
#define CTRL_EDGE           0x10
#define CTRL_PRESCALER      0x20
#define CTRL_MODE           0x40
#define CTRL_INTERRUPT      0x80

static VOID CTCCounterInit(struct CTCCounterData *data)
{
    data->ctr_OutputPulse = FALSE;
    data->ctr_InterruptPending = FALSE;
    data->ctr_Prescaler = 0;
    data->ctr_WaitForTimeConstraint = FALSE;
    data->ctr_Control = 0;
    data->ctr_CountEnabled = FALSE;
    data->ctr_Decrement = FALSE;
    data->ctr_TimeConstant = 0;
    data->ctr_DownCounter = 0;
}

static VOID CTCCounterReset(struct CTCCounterData *data, BOOL hard)
{
    if(hard)
    {
        data->ctr_OutputPulse = FALSE;
        data->ctr_InterruptPending = FALSE;
        data->ctr_WaitForTimeConstraint = FALSE;
        // Terminate all down-counts
        data->ctr_DownCounter = 0;
        // Disable interrupts
        data->ctr_Control &= 0x7F;
        data->ctr_Enabled = FALSE;
    }
    else
    {
        // Stop counting
        data->ctr_CountEnabled = FALSE;
    }
}

static BOOL CTCCounterIsOutputPulse(struct CTCCounterData *data)
{
    return data->ctr_OutputPulse;
}

static BOOL CTCCounterIsIntPending(struct CTCCounterData *data)
{
    return data->ctr_InterruptPending;
}

static VOID CTCCounterClearInt(struct CTCCounterData *data)
{
    data->ctr_InterruptPending = FALSE;
}

static VOID CTCCounterSetTrigger(struct CTCCounterData *data, BOOL state)
{
    if(((data->ctr_Control & CTRL_EDGE) && state)
    || ((data->ctr_Control & CTRL_EDGE) == 0 && (!state)))
    {
        data->ctr_Decrement = TRUE;
    }
    if(!data->ctr_CountEnabled)
    {
        data->ctr_Prescaler = (data->ctr_Control & CTRL_PRESCALER) ? 256 : 16;
        data->ctr_CountEnabled = TRUE;
        //data->ctr->DownCounter = data->ctr_TimeConstant;
        data->ctr_Reload = TRUE;
    }
}

static VOID CTCCounterOut(struct CTCCounterData *data, UBYTE value)
{
    if(data->ctr_WaitForTimeConstraint)
    {
        data->ctr_TimeConstant = (value==0)?256:value;
        data->ctr_WaitForTimeConstraint = FALSE;

        data->ctr_CountEnabled = ((data->ctr_Control & CTRL_TIMER)==0);

        // todo : if data->ctr_CountEnabled, add shift for counter (compute from write cycle to T2 of next M1 operation

        // If timer mode & CTRL_TIMER, trigger it
        if(data->ctr_CountEnabled)
        {
            data->ctr_Prescaler = (data->ctr_Control & CTRL_PRESCALER) ? 256 : 16;
            //data->ctr_DownCounter = data->ctr_TimeConstant;
            data->ctr_Reload = TRUE;
        }
        data->ctr_Enabled = TRUE;
    }
    else
    {
        data->ctr_Control = value;

        data->ctr_WaitForTimeConstraint = data->ctr_Control & CTRL_TIME_CST;
        data->ctr_Enabled = TRUE;

        // Something to set ?
        if(data->ctr_Control & CTRL_RESET)
        {
            // Software reset
            CTCCounterReset(data, FALSE);
        }
    }
}

static VOID CTCCounterTick(struct CTCCounterData *data)
{
    data->ctr_OutputPulse = FALSE;

    // Timer mode
    if(data->ctr_CountEnabled && (data->ctr_WaitForTimeConstraint == FALSE) && data->ctr_Enabled)
    {
        if(data->ctr_Reload)
        {
            data->ctr_DownCounter = data->ctr_TimeConstant;
            data->ctr_Reload = FALSE;
        }
        else
        {
            if(data->ctr_Control & CTRL_MODE)
            {
                // Decrement ?
                if(data->ctr_Decrement)
                {
                    data->ctr_Decrement = FALSE;
                    --data->ctr_DownCounter;
                }
            }
            else
            {
                --data->ctr_Prescaler;
                if(data->ctr_Prescaler == 0)
                {
                    data->ctr_Prescaler = (data->ctr_Control & CTRL_PRESCALER) ? 256 : 16;
                    --data->ctr_DownCounter;
                }
            }
        }
        if(data->ctr_DownCounter == 0)
        {
            data->ctr_Reload = TRUE;

            // Trigger and interrupt
            if(data->ctr_Control & CTRL_INTERRUPT)
            {
                data->ctr_InterruptPending = TRUE;
            }
            data->ctr_OutputPulse = TRUE;
        }
    }
}

VOID CTCInit(struct CTCData *data)
{
    for(int i=0; i<CTC_CHANNEL_COUNT; i++)
    {
        CTCCounterInit(&data->ctc_Channel[i]);
    }
}

VOID CTCHardReset(struct CTCData *data)
{
    // IE0 = IEI
    for(int i=0; i<CTC_CHANNEL_COUNT; i++)
    {
        CTCCounterReset(&data->ctc_Channel[i], TRUE);
    }
}

VOID CTCSetTrigger(struct CTCData *data, UBYTE channel, BOOL state)
{
    if(channel < CTC_CHANNEL_COUNT)
    {
        CTCCounterSetTrigger(&data->ctc_Channel[channel], state);
    }
}

VOID CTCTick(struct CTCData *data)
{
    for(int i=0; i<CTC_CHANNEL_COUNT; i++)
    {
        CTCCounterTick(&data->ctc_Channel[i]);
    }
}

BOOL CTCIsOutputPulse(struct CTCData *data, UBYTE channel)
{
    // Last channel has no output
    if(channel < CTC_CHANNEL_COUNT-1)
    {
        return CTCCounterIsOutputPulse(&data->ctc_Channel[channel]);
    }
    else
    {
        return FALSE;
    }
}

BOOL CTCIsIntPending(struct CTCData *data, UBYTE *ivr)
{
    for(int i=0; i<CTC_CHANNEL_COUNT; i++)
    {
        if(CTCCounterIsIntPending(&data->ctc_Channel[i]))
        {
            if(ivr)
            {
                *ivr = data->ctc_InterruptVector | (i << 1);
            }
            return TRUE;
        }
    }
    return FALSE;
}

VOID CTCClearInt(struct CTCData *data)
{
    for(int i=0; i<CTC_CHANNEL_COUNT; i++)
    {
        if(CTCCounterIsIntPending(&data->ctc_Channel[i]))
        {
            CTCCounterClearInt(&data->ctc_Channel[i]);
            return;
        }
    }
}

// address : 2 bits : CS0, CS1
VOID CTCOut(struct CTCData *data, USHORT address, UBYTE value)
{
   // Send control word to proper channel
   if(address == 0 && ((value & 0x1) == 0) && (data->ctc_Channel[0].ctr_WaitForTimeConstraint == FALSE))
   {
        data->ctc_InterruptVector = value & 0xf8;
   }
   else
   {
        CTCCounterOut(&data->ctc_Channel[address & 0x3], value);
   }
}

VOID CTCIn(struct CTCData *data, USHORT address, UBYTE *value)
{
    // Timer mode?
    if(data->ctc_Channel[address & 0x3].ctr_CountEnabled && (data->ctc_Channel[address & 0x3].ctr_WaitForTimeConstraint == FALSE) && data->ctc_Channel[address & 0x3].ctr_Enabled)
    {
		*value = data->ctc_Channel[address & 0x3].ctr_DownCounter;
    }
	else
	{
		*value = 0;
	}
}
