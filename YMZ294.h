#ifndef LOCAL_YMZ294_H
#define LOCAL_YMZ294_H

/*
 * (C) Copyright 2017 Thomas GUILLEMIN
 *
 * Converted to plain C and improved by Philippe RIMAURO (c) 2020
 */

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#define YM_CLOCK_DIV 0

struct YMData
{
    // Actual audio outputs
    USHORT ym_OutputA;
    USHORT ym_OutputB;
    USHORT ym_OutputC;

    // Registers - 12 bits for theses
    UBYTE ym_Register[16];
    UBYTE ym_RegisterAddress;

    ULONG ym_ChannelAFreq;
    ULONG ym_ChannelAFreqCounter;

    ULONG ym_ChannelBFreq;
    ULONG ym_ChannelBFreqCounter;

    ULONG ym_ChannelCFreq;
    ULONG ym_ChannelCFreqCounter;

    ULONG ym_NoiseFrequency;
    ULONG ym_MixerControlRegister;

    UBYTE ym_ChannelAVolume;
    UBYTE ym_ChannelBVolume;
    UBYTE ym_ChannelCVolume;

    UBYTE ym_EnveloppeVolume;

    ULONG ym_VolumeEnveloppeFrequency;
    UBYTE ym_VolumeEnveloppeShape;

    // Envelope
    BOOL ym_Up;
    BOOL ym_Stop;

    /////////////////////////////////
    // Precise sound output
    ULONG ym_CounterA;
    ULONG ym_CounterB;
    ULONG ym_CounterC;
    ULONG ym_CounterNoise;
    ULONG ym_CounterEnv;
    ULONG ym_CounterStateEnv;

    UBYTE ym_ChannelAHigh;
    UBYTE ym_ChannelBHigh;
    UBYTE ym_ChannelCHigh;
    UBYTE ym_ChannelNoiseHigh;

    ULONG ym_NoiseShiftRegister;
};

VOID YMInit(struct YMData *data);

VOID YMReset(struct YMData *data);
VOID YMAccess(struct YMData *data, UBYTE *data_bus, UBYTE bus_ctrl);
VOID YMTick(struct YMData *data);

#endif
