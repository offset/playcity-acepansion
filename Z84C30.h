#ifndef LOCAL_Z84C30_H
#define LOCAL_Z84C30_H

/*
 * (C) Copyright 2017 Thomas GUILLEMIN
 *
 * Converted to plain C and improved by Philippe RIMAURO (c) 2020
 */

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#define CTC_CHANNEL_0 0
#define CTC_CHANNEL_1 1
#define CTC_CHANNEL_2 2
#define CTC_CHANNEL_3 3

#define CTC_CHANNEL_COUNT 4

struct CTCCounterData
{
    BOOL ctr_Enabled;
    UBYTE ctr_Control;
    UBYTE ctr_TimeConstant;
    UBYTE ctr_DownCounter;
    UBYTE ctr_Prescaler;
    BOOL ctr_Decrement;
    BOOL ctr_WaitForTimeConstraint;
    BOOL ctr_CountEnabled;
    BOOL ctr_Reload;
    BOOL ctr_OutputPulse;
    BOOL ctr_InterruptPending;
};

struct CTCData
{
    UBYTE ctc_InterruptVector;
    struct CTCCounterData ctc_Channel[4];
};

VOID CTCInit(struct CTCData *data);

VOID CTCHardReset(struct CTCData *data);
VOID CTCTick(struct CTCData *data);

BOOL CTCIsOutputPulse(struct CTCData *data, UBYTE channel);
BOOL CTCIsIntPending(struct CTCData *data, UBYTE *ivr);
VOID CTCClearInt(struct CTCData *data);

VOID CTCSetTrigger(struct CTCData *data, UBYTE channel, BOOL state);

// Address : CS0, CS1;
VOID CTCOut(struct CTCData *data, USHORT address, UBYTE value);
VOID CTCIn(struct CTCData *data, USHORT address, UBYTE *value);

#endif
