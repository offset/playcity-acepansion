#ifndef LOCAL_PLAYCITY_H
#define LOCAL_PLAYCITY_H

/*
 * (C) Copyright 2017 Thomas GUILLEMIN
 *
 * Converted to plain C and improved by Philippe RIMAURO (c) 2020
 */

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#include "YMZ294.h"
#include "Z84C30.h"

struct PlayCityData
{
    // A Playcity is :
    // - 1 Z84C30
    struct CTCData pc_CTC;
    // - 2 YMZ294
    struct YMData pc_YM1;
    struct YMData pc_YM2;

    // YM count to avoid multiple & useless calls
    LONG pc_NextCallYM;

    BOOL pc_Clock;
};

VOID PlayCityInit(struct PlayCityData *data);

VOID PlayCityReset(struct PlayCityData *data);

VOID PlayCityTick(struct PlayCityData *data, BOOL cursorPulse, BOOL *intPending, BOOL *nmiPending);

BOOL PlayCityGetIVR(struct PlayCityData *data, UBYTE *ivr);
VOID PlayCityClearInt(struct PlayCityData *data);

VOID PlayCityOut(struct PlayCityData *data, USHORT address, UBYTE value);
VOID PlayCityIn(struct PlayCityData *data, USHORT address, UBYTE *value);

#endif
