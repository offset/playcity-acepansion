#include "PlayCity.h"

#include "YMZ294.h"
#include "Z84C30.h"

#define YM_CALL (16 >> YM_CLOCK_DIV)

VOID PlayCityInit(struct PlayCityData *data)
{
    YMInit(&data->pc_YM1);
    YMInit(&data->pc_YM2);

    CTCInit(&data->pc_CTC);
}

VOID PlayCityReset(struct PlayCityData *data)
{
    CTCHardReset(&data->pc_CTC);
    YMReset(&data->pc_YM1);
    YMReset(&data->pc_YM2);

    data->pc_Clock = FALSE;

    data->pc_NextCallYM = YM_CALL;
}

VOID PlayCityTick(struct PlayCityData *data, BOOL cursorPulse, BOOL *intPending, BOOL *nmiPending)
{
    // Channel 0/2 input is the 4MHz clock
    CTCSetTrigger(&data->pc_CTC, CTC_CHANNEL_0, data->pc_Clock);
    CTCSetTrigger(&data->pc_CTC, CTC_CHANNEL_2, data->pc_Clock);
    data->pc_Clock = !data->pc_Clock;

    // Channel 1 input is the cursor pulse
    if(cursorPulse)
    {
        CTCSetTrigger(&data->pc_CTC, 1, TRUE);
    }

    CTCTick(&data->pc_CTC);

    // Channel 0 output is the YMs clock
    if(CTCIsOutputPulse(&data->pc_CTC, CTC_CHANNEL_0))
    {
        if(--data->pc_NextCallYM == 0)
        {
            YMTick(&data->pc_YM1);
            YMTick(&data->pc_YM2);

            data->pc_NextCallYM = YM_CALL;
        }
    }

    // Channel 1 output generates NMI
    if(CTCIsOutputPulse(&data->pc_CTC, CTC_CHANNEL_1))
    {
        *nmiPending = TRUE;
    }

    // Channel 2 output is channel 3 trigger
    if(CTCIsOutputPulse(&data->pc_CTC, CTC_CHANNEL_2))
    {
        CTCSetTrigger(&data->pc_CTC, CTC_CHANNEL_3, TRUE);
    }

    // Handle interrupts
    if(CTCIsIntPending(&data->pc_CTC, NULL))
    {
        *intPending = TRUE;
    }
}

BOOL PlayCityGetIVR(struct PlayCityData *data, UBYTE *ivr)
{
    return CTCIsIntPending(&data->pc_CTC, ivr);
}

VOID PlayCityClearInt(struct PlayCityData *data)
{
    CTCClearInt(&data->pc_CTC);
}

VOID PlayCityOut(struct PlayCityData *data, USHORT address, UBYTE value)
{
    // 1111 100x 1000 xxxx
    // F880 -> F883 = Z84C30
    if((address & 0xFEFF) == 0xF880)
    {
        CTCOut(&data->pc_CTC, 0, value);
    }
    else if((address & 0xFEFF) == 0xF881)
    {
        CTCOut(&data->pc_CTC, 1, value);
    }
    else if((address & 0xFEFF) == 0xF882)
    {
        CTCOut(&data->pc_CTC, 2, value);
    }
    else if((address & 0xFEFF) == 0xF883)
    {
        CTCOut(&data->pc_CTC, 3, value);
    }
    // YMZ294 : Write (F884 = right, F888 = left)
    else if((address & 0xFFFF) == 0xF884)
    {
        YMAccess(&data->pc_YM1, &value, 0x2);
    }
    else if((address & 0xFFFF) == 0xF888)
    {
        YMAccess(&data->pc_YM2, &value, 0x2);
    }
    // YMZ : Select (F984  = right, F988 = left)
    else if((address & 0xFFFF) == 0xF984)
    {
        YMAccess(&data->pc_YM1, &value, 0x3);
    }
    else if ((address & 0xFFFF) == 0xF988)
    {
        YMAccess(&data->pc_YM2, &value, 0x3);
    }
}

VOID PlayCityIn(struct PlayCityData *data, USHORT address, UBYTE *value)
{
    // No Output from Playcity
    if((address & 0xFEFF) == 0xF880)
    {
        CTCIn(&data->pc_CTC, 0, value);
    }
    else if ((address & 0xFEFF) == 0xF881)
    {
        CTCIn(&data->pc_CTC, 1, value);
    }
    else if((address & 0xFEFF) == 0xF882)
    {
        CTCIn(&data->pc_CTC, 2, value);
    }
    else if ((address & 0xFEFF) == 0xF883)
    {
        CTCIn(&data->pc_CTC, 3, value);
    }
    // YMZ294 : Write (F884 = right, F888 = left)
    else if((address & 0xFFFF) == 0xF884)
    {
        YMAccess(&data->pc_YM1, value, 0x2);
    }
    else if((address & 0xFFFF) == 0xF888)
    {
        YMAccess(&data->pc_YM2, value, 0x2);
    }
    // YMZ : Select (F984  = right, F988 = left)
    else if((address & 0xFFFF) == 0xF984)
    {
        YMAccess(&data->pc_YM1, value, 0x3);
    }
    else if((address & 0xFFFF) == 0xF988)
    {
        YMAccess(&data->pc_YM2, value, 0x3);
    }
}

